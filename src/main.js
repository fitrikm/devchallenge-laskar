// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/scss/app.scss'
import 'edu-vue-event-calendar/dist/style.css'
import vueEventCalendar from 'edu-vue-event-calendar'
import router from './router'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(vueEventCalendar, {locale: 'en', color: '#53b9ec'})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
