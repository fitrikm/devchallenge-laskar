import Vue from 'vue'
import Router from 'vue-router'
import Greeting from '@/views/Greeting'
import Project from '@/views/Project'
import TeamPerformance from '@/views/TeamPerformance'
import Calendar from '@/views/Calendar'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Greeting',
      component: Greeting
    },
    {
      path: '/project',
      name: 'Project',
      component: Project
    },
    {
      path: '/team-performance',
      name: 'TeamPerformance',
      component: TeamPerformance
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: Calendar
    }
  ]
})
