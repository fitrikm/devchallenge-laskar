# devchallenge-laskar

> Telkom Developer Challenge Project

## Laskar Team
``` bash
Members :
1. Dede Rusliandi, Back-End Developer
2. Fitri Kurnia Mulyani, Front-End Developer

```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
